#include <iostream>

int MAX = 45;

int fib(int f)
{
	int result = 0;
	if (f < 2)
		return f;

	return  fib(f - 1) + fib(f - 2);
}
int main (int argc, char *argv[])
{
	int results[MAX];

	long result = 0;
	for (int i = 0; i < MAX; ++i)
	{
		result = fib(i);
		results[i] = result;
		std::cout << "result : " <<  result << " " << i << '\n';
		result = 0;
	}

	std::cout << results[44];

	return 0;
}
