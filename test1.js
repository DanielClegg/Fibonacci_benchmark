var MAX = 45;

fib = function(f) {
    var result = 0;
    if (f < 2)
        return f;

    return fib(f - 1) + fib(f - 2);
};

main = function() {
    var results = [MAX];
    var result = 0;
    for (var i = 0; i < MAX; ++i) {
        result = fib(i);
        results[i] = result;
        console.log("result  : " + result + " " + i);
        
        result = 0;
    }

    return 0;
};

main();