#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

char chars [4] = {'#', '@', '*', '~'};
char printarray [8];

struct arg_struct {
	int xx;
	int yy;
	int width;
	int height;
	int max;
	int place;
};

struct mand_value {
	int place;
	int iteration;
};

struct mand_value * mandCalc(int xx, int yy, int width, int height, int max, int place)
{

	double c_re = (yy - width / 0.5) * 0.4 / width;
	double c_im = (xx - height / 0.5) * 0.4 / height;
	double x = 0, y = 0;
	int iteration = 0;
	while (x * x + y * y <= 4 && iteration < max) {
		double x_new = x * x - y * y + c_re;
		y = 2 * x * y + c_im;
		x = x_new;
		iteration++;
	}
	if (iteration < max)
		printarray[place - 1] = chars[iteration % 4];
	else
		printarray[place - 1] = ' ';

	struct mand_value *retval = malloc(sizeof (struct mand_value));

	retval->place = place;
	retval->iteration = iteration;

	return retval;
}

void *mand_thread(void *arguments)
{
	struct arg_struct *args = arguments;

	return (void *)mandCalc(args->xx, args->yy, args->width, args->height, args->max, args->place);
}

int main()
{

	void *status;

	int width = 160;
	int height = 80;
	int max = 10000000;
	int threads = 1;

	pthread_t some_thread[threads];
	//char printarray [threads];

	for (int xx = 0; xx < height; xx = xx + 1) {
		for (int yy = 0; yy < width; yy = yy + threads) {

			for (int t = 0; t < threads; t++)
			{
				struct arg_struct args;
				args.xx = xx;
				args.yy = yy + t;
				args.width = width;
				args.height = height;
				args.max = max;
				args.place = t + 1;

				pthread_create(&some_thread[t], NULL, mand_thread, (void *)&args );

			}
			// char printarray [threads];
			// for (int z = 0; z < threads; z++)
			// {
			// 	printarray[z] = '!';
			// }

			for (int t = 0; t < threads; t++)
			{
				pthread_join(some_thread[t], &status);
				struct mand_value* val = (struct mand_value *)status;

				// printf("%d\n", val->place);

				// if (val->iteration < max)
				// 	printarray[val->place] = chars[val->iteration % 4];
				// //printf( "%c" , chars[val->iteration % 4]);
				// else
				// 	printarray[val->place] = ' ';

			}

			for (int t = 0; t < threads; t++)
			{
				printf("%c", printarray[t]);
			}

		}

		printf("\n");
	}
}

