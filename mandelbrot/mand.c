#include <stdio.h>

int main()
{
	char chars [4] = {'#', '@', '*', '~'};

	int width = 160;
	int height = 80;
	int max = 10000000;

	for (int xx = 0; xx < height; xx++) {
		for (int yy = 0; yy < width; yy++) {
			double c_re = (yy - width / 0.5) * 0.4 / width;
			double c_im = (xx - height / 0.5) * 0.4 / height;
			double x = 0, y = 0;
			int iteration = 0;
			while (x * x + y * y <= 4 && iteration < max) {
				double x_new = x * x - y * y + c_re;
				y = 2 * x * y + c_im;
				x = x_new;
				iteration++;
			}
			if (iteration < max)
				printf( "%c" ,chars[iteration % 4]);
			else
				printf(" ");
		}
		printf("\n");
	}
}
