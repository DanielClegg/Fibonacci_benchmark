gcc test1.c  -o test1notop
gcc test1.c -O -o test1OO
gcc test1.c -O1 -o test1O1
gcc test1.c -O2 -o test1O2
gcc test1.c -O3 -o test1O3
g++ test1.cpp -o testcpp
g++ test1.cpp -o testcppOO -O 
g++ test1.cpp -o testcppO1 -O1
g++ test1.cpp -o testcppO2 -O2
g++ test1.cpp -o testcppO3 -O3
dmcs test1.cs

echo "No op"
time ./test1notop
echo "op 0"
time ./test1OO
echo "op 1"
time ./test1O1
echo "op 2"
time ./test1O2
echo "op 3"
time ./test1O3

echo "C++ No op"
time ./testcpp
echo "C++ op 0"
time ./testcppOO
echo "C++ op 1"
time ./testcppO1
echo "C++ op 2"
time ./testcppO2
echo "C++ op 3"
time ./testcppO3
echo "c#"
time mono test1.exe
echo "python"
time python test1.py
echo "Node"
time node test1.js
echo "Swift"
time swift test1.swift