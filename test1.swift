//: Playground - noun: a place where people can play

//import UIKit

var str = "Hello, playground"

var MAX: Int = 45
var result : Int = 0

func fib(f: Int) -> Int
{
    if (f < 2)
    {
        return f
    }
    
    return fib(f-1) + fib(f - 2)
}

for i in 1...MAX
{
    result = fib(i)
    
    print("result : \(result)")
}
